import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-c2',
  templateUrl: './c2.component.html',
  styleUrls: ['./c2.component.css']
})
export class C2Component implements OnInit {

  public successClass = 'text-success';

  public hasError = true;
  public isSpecial = true;

  public messageClasses = {
    'text-success': !this.hasError,
    'text-erreur': this.hasError,
    'text-special': this.isSpecial
  };

  public highlightColor = 'orange';

  public titleStyles = {
    color: 'blue',
    fontStyle: 'italic'
  };

  constructor() { }

  ngOnInit() {
  }

}
