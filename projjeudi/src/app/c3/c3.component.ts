import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-c3',
  templateUrl: './c3.component.html',
  styleUrls: ['./c3.component.css']
})
export class C3Component implements OnInit {

  public salut1 = "";
  public salut2 = "";

  onClick(event) {
    console.log('Salut !');
    console.log(event);
    this.salut1 = 'Salut !';
    this.salut2 = event.type;
  }

  constructor() { }

  ngOnInit() {
  }

}
