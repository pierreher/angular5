import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HeroListComponent} from "./hero-list/hero-list.component";
import {CityListComponent} from "./city-list/city-list.component";
import {NotFoundComponent} from "./not-found/not-found.component";

const routes: Routes = [
  { path: 'heroes', component: HeroListComponent },
  { path: 'cities', component: CityListComponent },
  { path: '', component: HeroListComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
export const routingComponents = [HeroListComponent, CityListComponent, NotFoundComponent];
