import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import {Todo} from "./todo";
import {catchError, map, tap} from "rxjs/operators";

@Injectable()
export class TodoService {

  constructor(private http: HttpClient) { }

  // private todosUrl = './mock-todos.json';
  private todosUrl = 'https://jsonplaceholder.typicode.com/todos';

  getTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.todosUrl)
  }

  getOneTodo(todoId: number): Observable<Todo> {
    const url = `${this.todosUrl}/${todoId}`;
    console.log('url : '+url);
    return this.http.get<Todo>(url);
  }

  updateTodo(todo : Todo): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    };

    return this.http.put(this.todosUrl, todo, httpOptions);
  }

}
