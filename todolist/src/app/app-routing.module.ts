import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TodosComponent} from "./todos/todos.component";
import { AccueilComponent} from "./accueil/accueil.component";
import { TodoformComponent } from "./todoform/todoform.component";

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'todos', component: TodosComponent },
  { path: 'todos/:todoId', component: TodoformComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [AccueilComponent, TodosComponent, TodoformComponent];

