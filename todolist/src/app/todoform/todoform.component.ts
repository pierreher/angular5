import {Component, OnInit, Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Todo} from "../todo";
import {TodoService} from "../todo.service";
import { Location } from "@angular/common";

@Component({
  selector: 'app-todoform',
  templateUrl: './todoform.component.html',
  styleUrls: ['./todoform.component.css']
})
export class TodoformComponent implements OnInit {

  @Input() todo: Todo;

  constructor(
    private route: ActivatedRoute,
    private todoService: TodoService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getOneTodo();
  }

  getOneTodo(): void {
    const id = +this.route.snapshot.paramMap.get('todoId');
    console.log('id = '+id);
    this.todoService.getOneTodo(id)
      .subscribe(todo => this.todo = todo);
  }

  save(): void {
    this.todoService.updateTodo(this.todo);
    console.log(this.todo);
    //   .subscribe(() => 'http://127.0.0.1/');
  }

  goBack(): void {
    this.location.back();
  }
}
