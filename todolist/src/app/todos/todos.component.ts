import { Component, OnInit } from '@angular/core';
import { Todo } from "../todo";
import { TodoService } from "../todo.service";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { FormsModule } from "@angular/forms";


@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todosList: Todo[];
  oneTodo: Todo;

  constructor(private todoService: TodoService) { }

  getTodos(): Observable<Todo[]> {
    this.todoService.getTodos()
      .subscribe(todos => this.todosList = todos);
    return of(this.todosList);
  }

  // getOneTodo(todoId): Observable<Todo> {
  //   this.todoService.getOneTodo(todoId)
  //     .subscribe(todo => this.oneTodo = todo);
  //   return of(this.oneTodo);
  // }

  ngOnInit() {
    this.getTodos();
  }

}
