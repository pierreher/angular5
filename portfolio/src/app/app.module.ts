import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CompetencesComponent } from './pages/competences/competences.component';
import { ExpComponent } from './pages/exp/exp.component';
import { FormationComponent } from './pages/formation/formation.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {PresentationComponent} from './pages/presentation/presentation.component';
import {ContactComponent} from './pages/contact/contact.component';

const routeApp: Routes = [
  { path: '', component: PresentationComponent},
  { path: 'comp', component: CompetencesComponent },
  { path: 'exp', component: ExpComponent },
  { path: 'form', component: FormationComponent },
  { path: 'contact', component: ContactComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CompetencesComponent,
    ExpComponent,
    FormationComponent,
    PresentationComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routeApp)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
