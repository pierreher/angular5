import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  title ='Batman';
  city = 'Gotham City';
  public url = window.location.href;


  constructor() {
  }

  ngOnInit() {
  }

}
