import { Injectable } from '@angular/core';
import { Film } from './films';
import { Observable } from "rxjs/Observable";
import { of } from 'rxjs/observable/of';
import { FILMS } from "./mock-todos";
import { MessageService } from "./message.service";
import { catchError, map, tap } from "rxjs/operators";

import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class FilmService {

  private filmsUrl = 'api/films'; //URL to web api

  constructor(private http: HttpClient,
              private messageService: MessageService) { }

  private handleError<T> (operation = 'operation',result?: T) {
    return (error:any): Observable<T> => {
      //console.error(error);
      this.log(`${operation} failed: ${error.message} `);

      return of(result as T);
    }
  }

  getFilms(): Observable<Film[]> {
    this.messageService.add('Films trouvés.');
    // return of(FILMS);
    return this.http.get<Film[]>(this.filmsUrl)
      .pipe(
        tap(films => this.log(`films trouvés`)),
        catchError(this.handleError('getFilms',[]))
      );
  }

  getFilm(id: number): Observable<Film> {
    const url = `${this.filmsUrl}/${id}`;
    return this.http.get<Film>(url)
      .pipe(
      tap(_ => this.log(`film à l\'id ${id} trouvé`)),
      catchError(this.handleError<Film>(`getFilm id=${id}`))
    )
  }

  /* PUT : updates the film on the server */
  updateFilm (film: Film): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    return this.http.put(this.filmsUrl, film, httpOptions).pipe(
      tap(_ => this.log(`film à l\'id ${film.id} modifié.`)),
      catchError(this.handleError<any>('updateFilm'))
    );
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add('FilmService: ' + message);
  }

}
