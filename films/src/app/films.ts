export class Film {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}
