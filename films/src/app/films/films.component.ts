import { Component, OnInit } from '@angular/core';
import { Film } from "../films";
import { FilmService } from "../film.service";
import { MessageService } from "../message.service";

@Component({ //Decorator
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {

  films: Film[];

  selectedFilm: Film;

  //Initialisation du service
  constructor(private filmService: FilmService, private messageService: MessageService) { }

  //Appel des getters/setters
  ngOnInit() {
    this.getFilms();
    //appeller getFilms() ici laisse Angular l'appeler au bon moment APRES que l'instance soit construite
  }

  onSelect(film: Film): void {
    this.selectedFilm = film;
    this.messageService.clear();
    this.messageService.add('Vous consultez le film : '+this.selectedFilm.title);
  }

  //Déclaration des getters setters définis dans FilmService
  getFilms(): void {
    this.filmService.getFilms()
      .subscribe(films => this.films = films);
  }


}
