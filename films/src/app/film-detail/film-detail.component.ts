import { Component, OnInit, Input } from '@angular/core';
import { Film } from '../films';
import { FilmService } from "../film.service";

@Component({
  selector: 'app-film-detail',
  templateUrl: './film-detail.component.html',
  styleUrls: ['./film-detail.component.css']
})

export class FilmDetailComponent implements OnInit {

  @Input() film: Film;
  filmService: FilmService;

  constructor() { }

  save(): void {
    this.filmService.updateFilm(this.film)
      .subscribe(/*() => this.location.back()*/);
  }

  ngOnInit() {
  }

}
