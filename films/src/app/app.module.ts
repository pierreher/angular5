import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { FilmsComponent } from './films/films.component';
import { FilmDetailComponent } from "./film-detail/film-detail.component";
import { FilmService } from './film.service';
import { MessagesComponent } from "./messages/messages.component";
import { MessageService } from "./message.service";

//HTTP Services
import { HttpClientInMemoryWebApiModule } from "angular-in-memory-web-api";
import { InMemoryDataService } from "./in-memory-data.service";
import {HttpClientModule} from "@angular/common/http";

//Routes

@NgModule({
  declarations: [
    AppComponent,
    FilmsComponent,
    FilmDetailComponent,
    MessagesComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],

  providers: [
    FilmService,
    MessageService,
    InMemoryDataService
  ],

  bootstrap: [AppComponent ]

})
export class AppModule { }
